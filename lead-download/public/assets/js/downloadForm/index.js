import axios from '../util/axios';
import jsontocsv from '../util/jsontocsv';

const formElement = document.querySelector('#form');
const submitButton = document.querySelector('#submitButton');
const formTitleElement = document.querySelector('#formTitle');
const inputCode = document.querySelector('#inputCode');

const pathname = window.location.pathname.split('/').filter(p => !!p);
const [, email, lineItem, token ] = pathname;


function init() {
  enableForm();
}

function enableForm() {
  formElement.addEventListener('submit', sendForm);

  formTitleElement.innerHTML = `
    Digite abaixo o código de segurança para baixar os leads da campanha
    <b>${lineItem}</b>
  `;
}

function sendForm(event, creativeData) {
  event.preventDefault();
  /* Disable submit button */
  submitButton.disabled = true;

  const data = {
    advertiser_email: email,
    campaign_dfp_line_item_id: lineItem,
    link_token: token,
    confirmation_code: event.target.downloadCode.value,
  }

  const options = {
      headers: {
        'Content-Type': 'application/json',
    }
  }
  axios.post('/submit-download-request', data, options)
    .then(({ data }) => {

      if(data.status === "SUCESSO") {
        success(data);
      }else if(data.status === "ERRO") {
        error(data.message);
      }

    })
    .catch(err => new Error(err))
    .finally(() => submitButton.disabled = false)

}

function success(data) {
  formElement.innerHTML = `
    <div class="alert-message show text-center mb-25">${data.message}</div>
  `;
  downloadCSV(data.content, data.file_name);
}

function downloadCSV(content, filename) {
  const csv = jsontocsv(content);
  const blob = new Blob([csv]);

  if(window.navigator.msSaveOrOpenBlob) {  // IE hack
    window.navigator.msSaveBlob(blob, "filename.csv");
  }else {
    const a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob, { type: "text/csv" });
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

function error(message) {
  const alertMessageElement = document.querySelector('.alert-message');

  alertMessageElement.innerHTML = message;
  alertMessageElement.classList.add('show');
}

export default {
  init,
}