import axios from '../util/axios';
import inputmask from '../util/inputmask';

const formElement = document.querySelector('#form');
const submitButton = document.querySelector('#submitButton');
const inputCampaignCode = document.querySelector('#campaignCode');

function init() {
  enableForm();
}

function enableForm() {
  formElement.addEventListener('submit', sendForm);
  inputmask.number(inputCampaignCode);
}

function sendForm(event, creativeData) {
  event.preventDefault();
  /* Disable submit button */
  submitButton.disabled = true;

  const data = {
    email: event.target.email.value,
    dfp_line_item_id: event.target.lineItem.value,
  };

  axios.get('/get-download-link/', {
      params: data,
    })
    .then(res => {
      const { message, status } = res.data;
      

      if(status === "SUCESSO") {
        success(message);
      }else if(status === "ERRO") {
        error(message);
      }

    })
    .catch(err => new Error(err))
    .finally(() => submitButton.disabled = false)

}

function success(message) {
  formElement.innerHTML = `
    <div class="alert-message show text-center">${message}</div>
  `
}

function error(message) {
  const alertMessageElement = document.querySelector('.alert-message');

  alertMessageElement.innerHTML = message;
  alertMessageElement.classList.add('show');
}

export default {
  init,
}