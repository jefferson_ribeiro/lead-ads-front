import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://leads.globo.com/api/',
});

export default instance;