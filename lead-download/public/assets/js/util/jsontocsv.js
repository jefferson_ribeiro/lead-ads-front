function replacer(key, value) {
  return value === null ? '' : value; 
}

function convert(json) {
  const header = Object.keys(json[0]);
   
  let csv = json.map(row => {
    return header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(',');
  });

  // add header column
  csv.unshift(header.join(','));
  
  return csv.join('\n');
}

export default convert;