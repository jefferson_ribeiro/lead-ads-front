function number(element) {
  element.addEventListener('keypress', e => {
    const validKeys = '0123456789';
    if(!validKeys.includes(e.key)) {
      e.preventDefault();
    }
  });
}

export default {
  number,
}