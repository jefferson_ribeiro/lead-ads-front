import downloadForm from './downloadForm/';
import requestLinkForm from './requestLinkForm/';

const bodyClasses = document.body.classList;

if(bodyClasses.contains('download-page')) {
  downloadPage();
}else if(bodyClasses.contains('request-link-page')) {
  requestLinkPage();
}

function downloadPage() {
  downloadForm.init();
}

function requestLinkPage() {
  requestLinkForm.init();
}