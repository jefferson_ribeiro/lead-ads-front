'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const concatcss = require('gulp-concat-css');
const autoprefixer = require('gulp-autoprefixer');
const uglifycss = require('gulp-uglifycss');
const browserify = require('gulp-browserify');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const webserver = require('gulp-webserver');

const paths = {
	SCSS: 'public/assets/scss/*.scss',
	CSS: ['public/assets/css/**/*.css', '!public/assets/css/bundle.css'],
	JS: {
		entry: 'public/assets/js/app.js',
		watch: ['public/assets/**/*.js', '!public/assets/js/bundle.js'],
	},
	IMG: ['public/assets/img/*', '!public/assets/img/min']
}

gulp.task('sass', function () {
	return gulp.src(paths.SCSS)
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest('public/assets/css'));
});

gulp.task('concat-css', function() {
	gulp.src(paths.CSS)
	.pipe(concatcss('bundle.css'))
	.pipe(gulp.dest('public/assets/css'));
});

gulp.task('concat-css:build', function() {
	gulp.src(paths.CSS)
	.pipe(concatcss('bundle.css'))
	.pipe(uglifycss({
		"uglyComments": true
	}))
	.pipe(gulp.dest('public/assets/css'));
});

gulp.task('browserify', function() {
	gulp.src(paths.JS.entry)
	.pipe(browserify({
		insertGlobals : true,
		debug : !gulp.env.production
	}))
	.pipe(rename('bundle.js'))
	.pipe(gulp.dest('public/assets/js'));
});

gulp.task('browserify:build', function() {
	gulp.src(paths.JS.entry)
	.pipe(browserify({
		insertGlobals : true,
		debug : !gulp.env.production
	}))
	.pipe(rename('bundle.js'))
	.pipe(uglify())
	.pipe(gulp.dest('public/assets/js'));
});

gulp.task('watch', function () {
	gulp.watch(paths.SCSS, ['sass']);
	gulp.watch(paths.CSS, ['concat-css']);
	gulp.watch(paths.JS.watch, ['browserify']);
});

gulp.task('webserver', function() {
  gulp.src('./public/')
    .pipe(webserver({
			https: true,
      livereload: {
        enable: false, // need this set to true to enable livereload
        filter: function(fileName) {
          if (fileName.match(/.map$|.scss$|.js$/)) { // exclude all source maps from livereload
            return false;
          } else {
            return true;
          }
        }
      }
    }));
});

gulp.task('build', ['sass', 'concat-css:build', 'browserify:build']);