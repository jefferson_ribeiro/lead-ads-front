import axios from '../util/axios';


const formElement = document.querySelector('#leadform');
const recaptchaElement = document.getElementById('recaptcha');
const termsLinkElement = document.querySelector('#termsLink');
const termsAcceptElement = document.querySelector('#termsAccept');

function init() {
  window.addEventListener('message', e => {
    if(e.data && !!e.data.creativeId) {
      enableForm(e.data);
    }
  });
}

function enableForm(creativeData) {
  formElement.addEventListener('submit', e => sendForm(e, creativeData));
  termsLinkElement.addEventListener('click', showTerms);
  termsAcceptElement.addEventListener('click', acceptTerms);
}

function sendForm(event, creativeData) {
  event.preventDefault();
  
  const data = mountFormData(event.target, creativeData);

  data.recaptcha_token = recaptchaElement.getAttribute('data-token');
  
  const options = {
      headers: {
        'Content-Type': 'application/json',
    }
  }
  axios.post('/new-lead-subscription/', data, options)
    .then(res => {
      const { message, status } = res.data;

      if(status === "SUCESSO") {
        success(message);
      }else if(status === "ERRO") {
        error(message);
      }

    })
    .catch(err => new Error(err));

}

function success(message) {
  const leadDone = document.querySelector('.lead-success');
  const formContainer = document.querySelector('.lead-form');
  formContainer.classList.remove('active');

  leadDone.classList.add('active');
  leadDone.innerHTML = `
    <p>${message}</p>
  `;
}

function error(message) {
  alert('error: ' + message);
}

function mountFormData(formEl, creativeData) {
  const {
    cid: creativeId,
    pid: adUnitId,
    aid: lineItemId,
    adv: advertiserId,
  } = creativeData;

  return {
    lead: {
      name: formEl.name.value,
      email: formEl.email.value,
      gender: formEl.gender.value,
    },
    campaign: {
      dfp_line_item_id: lineItemId,
      advertiser: {
        dfp_advertiser_id: advertiserId,
      },
    },
    dfp_ad_unit_id: adUnitId,
    dfp_creative_id: creativeId,
    term_acceptance: formEl.terms.checked,
  }
}

function showTerms(event) {
  event.preventDefault();
  document.querySelector('.lead-form.active').classList.remove('active');
  document.querySelector('.lead-terms').classList.add('active');
}

function acceptTerms() {
  document.querySelector('.lead-terms.active').classList.remove('active');
  document.querySelector('.lead-form').classList.add('active');

  formElement.terms.checked = true;
}

export default {
  init,
};