(function () {
  
  var APP_URL = 'https://leads.globo.com/new';
  var iframe = document.querySelector('iframe#app');


  if (Enabler.isInitialized()) {
    enablerInitHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
  }

  function fullscreenHandler(event) {
    // Enable expand button.
    if (event.supported) {
      document.querySelector('.expand-button').addEventListener('click', requestExpandHandler);
      document.getElementById('collapse-button').addEventListener('click', requestCollapseHandler);
    }
  }

  /* Click handler for expand button. */
  function requestExpandHandler(event) {
    Enabler.requestFullscreenExpand();
  }
  /* Click handler for collapse button. */
  function requestCollapseHandler(event) {
    Enabler.requestFullscreenCollapse();
  }
  /* Click handler for cta button. */
  function ctaClickHandler(event) {
    Enabler.exit('exit');
    Enabler.requestFullscreenCollapse();
  }

  /* Expand / Collapse lifecycle handler. */
  function expandStartHandler(event) {
    Enabler.finishFullscreenExpand();
    document.getElementById('expanded').classList.add('expand-animation');
    document.getElementById('collapsed').style.display = 'none';
    document.getElementById('expanded').style.display = 'block';
  }

  function expandFinishHandler(event) {
    var creativeData = JSON.parse(Enabler.getAdParameters()).a; 

    iframe.src = APP_URL;
    iframe.onload = function() {
      this.contentWindow.postMessage(creativeData, '*');
    }

    /* Avisa janela parent que criativo esta expandindo */
    creativeData.messageEvent = 'creativeExpanding';
    window.parent.postMessage(creativeData, '*');

  }

  function collapseStartHandler(event) {
    document.getElementById('expanded').classList.remove('expand-animation');
    document.getElementById('expanded').classList.add('collapse-animation');

    // Workaround for animation completion.
    setTimeout(function () {
      Enabler.finishFullscreenCollapse();
      document.getElementById('collapsed').style.display = 'block';
      document.getElementById('expanded').style.display = 'none';
      document.getElementById('expanded').classList.remove('collapse-animation');
    }, 300);
  }

  function collapseFinishtHandler(event) {
    iframe.src = '';

    
    var creativeData = JSON.parse(Enabler.getAdParameters()).a; 
    /* Avisa janela parent que criativo esta expandindo */
    creativeData.messageEvent = 'creativeCollapsing';
    window.parent.postMessage(creativeData, '*');
  }

  function enablerInitHandler(event) {
    Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_EXPAND_START, expandStartHandler);
    Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_EXPAND_FINISH, expandFinishHandler);
    Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_COLLAPSE_START, collapseStartHandler);
    Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_COLLAPSE_FINISH, collapseFinishtHandler);

    loadCreativeImage();


    /* Check for support of fullscreen. */
    Enabler.addEventListener(studio.events.StudioEvent.FULLSCREEN_SUPPORT, fullscreenHandler);
    Enabler.queryFullscreenSupport();
  }

  function loadCreativeImage() {
    var creativeAssets = JSON.parse(Enabler.getAdParameters()).a.assets.split('%26');
    var creativeImage = '';

    for(var i = 0; i < creativeAssets.length; i++) {
      if(!!creativeAssets[i].toLowerCase().match(/(.png|.gif|.jpeg|.jpg)$/)) {
        creativeImage = creativeAssets[i].split('%3D')[0];
        break;
      }
    }

    document.getElementById('collapsed').style.backgroundImage = 'url(' + creativeImage + ')';
  }

})();